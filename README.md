# OpenML dataset: Spotify---All-Time-Top-2000s-Mega-Dataset

https://www.openml.org/d/43386

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset contains audio statistics of the top 2000 tracks on Spotify. The data contains about 15 columns each describing the track and it's qualities. Songs released from 1956 to 2019 are included from some notable and famous artists like Queen, The Beatles, Guns N' Roses, etc.
http://sortyourmusic.playlistmachinery.com/ by plamere uses Spotify API to extract the audio features from the tracks given the Spotify Playlist URI. This data contains audio features like Danceability, BPM, Liveness, Valence(Positivity) and many more.
Each feature's description has been given in detail below.
Content

Index: ID
Title: Name of the Track
Artist: Name of the Artist
Top Genre: Genre of the track
Year: Release Year of the track
Beats per Minute(BPM): The tempo of the song
Energy: The energy of a song - the higher the value, the more energtic. song
Danceability: The higher the value, the easier it is to dance to this song.
Loudness: The higher the value, the louder the song.
Valence: The higher the value, the more positive mood for the song.
Length: The duration of the song.
Acoustic: The higher the value the more acoustic the song is.
Speechiness: The higher the value the more spoken words the song contains
Popularity: The higher the value the more popular the song is.

Acknowledgements
This data is extracted from the Spotify playlist - Top 2000s on PlaylistMachinery(plamere) using Selenium with Python. More specifically, it was scraped from http://sortyourmusic.playlistmachinery.com/. Thanks to Paul for providing a free and open source to extract features and do cool stuff with your Spotify playlists!
Inspiration
This is a very fun dataset to explore and find out unique links which land songs in the Top 2000s. With this dataset, I wanted to be able to answer some questions like:

Which genres were more popular coming through 1950s to 2000s?
Songs of which genre mostly saw themselves landing in the Top 2000s?
Which artists were more likely to make a top song?
Songs containing which words are more popular?
What is the average tempo of songs compared over the years?
Is there a trend of acoustic songs being popular back in 1960s than they are now?
Is there a trend in genres preferred back in the day vs now?
 and a lot more.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43386) of an [OpenML dataset](https://www.openml.org/d/43386). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43386/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43386/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43386/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

